SUMMARY = "Harry Austen's personal raspberry pi image"
LICENSE = "MIT"

# OpenSSH server
IMAGE_INSTALL += "openssh"
# GPIO library and tools
IMAGE_INSTALL += "libgpiod"

# Enable UART serial connectivity
ENABLE_UART = "1"
# Access to Das U-Boot bootloader
RPI_USE_U_BOOT = "1"

# Base image off of poky core image
inherit core-image
